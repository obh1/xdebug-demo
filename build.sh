#!/bin/bash
docker rm -f test
docker build -t test .
docker run -d --name test -p 80:80 -e DEVELOPER_HOST=${DEVELOPER_HOST:-host.docker.internal} test
