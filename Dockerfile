FROM php:apache
RUN apt-get update && apt-get install -y ncat
RUN pecl install xdebug \
 && docker-php-ext-enable xdebug
COPY xdebug.ini	/usr/local/etc/php/conf.d
COPY . /var/www/html
